terraform {
    backend "http"{

    }
    required_providers {
        gitlab = {
            source = "gitlabhq/gitlab"
            version = "~> 3.16.0"
        }
    }
}

provider "gitlab" {
    token = var.gitlab_access_token
}

data "gitlab_project" "my_project" {
    id = 37701914
}

resource "gitlab_project_variable" "my_project_variable"{
    project = data.gitlab_project.my_project.id
    key = "example_var"
    value = "Hello World!"
}
